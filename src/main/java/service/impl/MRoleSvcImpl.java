package service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.common.collect.Lists;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.google.gson.Gson;

import dao.MRoleDao;
import dao.MobSptDao;
import dto.MRoleDto;
import entity.MRole;
import entity.MobSpt;
import service.MRoleSvc;

@Service
@Transactional
public class MRoleSvcImpl implements MRoleSvc {
	
	public Boolean init = false;
	
	@Autowired
	private MRoleDao mRoleDao;
	
	@Autowired
	private MobSptDao mobSptDao;
	
	@Override
	public void save() {
		try{
			MRole r = new MRole();
			r.setRoleName("test sajoo");
			r.setCreatedBy("Admin");
			r.setCreatedDate(new Timestamp(System.currentTimeMillis()));
			r.setDescription("Test sajoo");
			r.setIsActive(true);
			r.setIsAdmin(false);
			r.setModifiedBy("Admin");
			r.setModifiedDate(new Timestamp(System.currentTimeMillis()));
			System.err.println("Data masuk "+new Date().getTime());
			mRoleDao.save(r);			
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public void tesAPI() {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			HashMap<String, Object> result = new HashMap<>();
			HttpResponse response;
			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpGet request = new HttpGet(
					"http://103.66.69.18:6026/tokenservices/getresttoken");
			JSONObject jobj = new JSONObject();
			try {
				response = httpClient.execute(request);

				HttpEntity entity = response.getEntity();
				String responseString;
				responseString = EntityUtils.toString(entity);
				jobj = new JSONObject(responseString);
				// HashMap<String, String> data = ;
				@SuppressWarnings("unchecked")
				HashMap<String, Object> yourHashMap = new Gson().fromJson(
						jobj.get("data").toString(), HashMap.class);
				String a = yourHashMap.get("token").toString();
				try {
					String url="http://103.66.69.18:6026/sptservices/mobspt/list";
					HttpClient client2 = new DefaultHttpClient();
					HttpPost post = new HttpPost(url);

					List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
					urlParameters.add(new BasicNameValuePair("token", a));

					post.setEntity(new UrlEncodedFormEntity(urlParameters));
					
					JSONObject jobj2 = new JSONObject();
					
					response = httpClient.execute(post);
					HttpEntity entity2 = response.getEntity();
					String responseString2 = EntityUtils.toString(entity2);
					
					
					jobj = new JSONObject(responseString2);
					
					
//					HashMap<String, Object> yourHashMap2 = new Gson().fromJson(
//							jobj.get("data").toString(), HashMap.class);
					
					JSONObject obj = jobj.getJSONObject("data");
					JSONArray joar = obj.getJSONArray("data");
					//List<ListDataApi> list = new ArrayList<>();
//					try {
//						 list = JsonUtil.mapJsonToListObject(joar, ListDataApi.class);
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
					
					List<MobSpt> listSpt = new ArrayList<>();
					
					for (int i = 0; i < joar.length(); i++) {
						MobSpt spt = new MobSpt();
						spt.setSptNo(Integer.parseInt(joar.getJSONObject(i).getString("spt_no"))) ;
						try {
							spt.setSptDate(sdf.parse(joar.getJSONObject(i).getString("spt_date")));
							spt.setSptExpiredDate(sdf.parse(joar.getJSONObject(i).getString("spt_expired_date")));
						} catch (JSONException | ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						spt.setSppbNo(joar.getJSONObject(i).getInt("sppb_no"));
						spt.setCustCode(joar.getJSONObject(i).getString("cust_code"));
						spt.setCustName(joar.getJSONObject(i).getString("cust_name"));
						spt.setAddressShipTo(joar.getJSONObject(i).getString("address_ship_to"));
						System.err.println("transportirCode = "+joar.getJSONObject(i).get("transportir_code"));
						if(joar.getJSONObject(i).get("transportir_code").toString().equalsIgnoreCase("null")||StringUtils.isEmpty(joar.getJSONObject(i).get("transportir_code"))){
							spt.setTransportirCode("Data Tidak Tersedia");
						}else{							
							spt.setTransportirCode((String) joar.getJSONObject(i).get("transportir_code"));
						}
						
						spt.setTransportirName(joar.getJSONObject(i).getString("transportir_name"));
						spt.setItemCode(joar.getJSONObject(i).getString("item_code"));
						spt.setItemName(joar.getJSONObject(i).getString("item_name"));
						spt.setQtySppb(new BigDecimal(joar.getJSONObject(i).getString("qty_sppb")));
						spt.setVehicleNo(joar.getJSONObject(i).getString("vehicle_no"));
						spt.setQty(new BigDecimal(joar.getJSONObject(i).getString("qty")));
						
						spt.setPhone(joar.getJSONObject(i).getString("phone"));
						String driverId=mobSptDao.findSPTIdPhone(spt.getPhone());
						if(driverId==null){
							System.err.println("Id tidak ditemukan");
						}else{
							spt.setDriverId(Integer.parseInt(driverId));
							System.err.println("driver id ditemukan, id = "+spt.getDriverId());
						}
						
						if(joar.getJSONObject(i).get("container_no") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("container_no"))){
							spt.setContainerNo("Data Tidak Tersedia");
						}else{
							spt.setContainerNo((String) joar.getJSONObject(i).get("container_no"));
						}
						spt.setWeightNetto(new BigDecimal(joar.getJSONObject(i).getString("weight_netto")));
						spt.setWeightBruto(new BigDecimal(joar.getJSONObject(i).getString("weight_bruto")));
						String status = joar.getJSONObject(i).getString("status");
						if(status.equalsIgnoreCase("open")){
							spt.setStatus("o");
						}else{
							spt.setStatus("c");
						}
						spt.setReferenceCode("1.0");
						spt.setCreated(joar.getJSONObject(i).getString("created"));
						spt.setCreatedDate(Timestamp.valueOf(joar.getJSONObject(i).getString("created_date")));
						spt.setModified(joar.getJSONObject(i).getString("created"));
						spt.setModifiedDate(Timestamp.valueOf(joar.getJSONObject(i).getString("created_date")));
						spt.setIsTransit(false);
						if(mobSptDao.countDataBySptNo(spt.getSptNo()) > 0){
							System.out.println("No Spt Sama = "+spt.getSptNo());
						}else{
							mobSptDao.save(spt);
							if(spt.getPhone() != null){
								getNotification(spt.getPhone());
							}else{
								System.err.println("No telepon kosong untuk spt = "+spt.getSptNo());
							}
							
							System.err.println("Data Tersimpan = "+spt.getSppbNo()+", "+new Date().getTime());
						}
					}
					
					
					
					result.put("status", 1);
					result.put("message", "sukses");
					result.put("object", a);
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					result.put("status", 0);
					result.put("message", "eror");
					result.put("object", e.toString());
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					result.put("status", 0);
					result.put("message", "eror");
					result.put("object", e.toString());
					e.printStackTrace();
				}

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				result.put("status", 0);
				result.put("message", "eror");
				result.put("object", e.toString());
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				result.put("status", 0);
				result.put("message", "eror");
				result.put("object", e.toString());
				e.printStackTrace();
			}

		}

	@Override
	public void getNotification(String noHp) {
		if(init==false){
			GoogleCredentials credentials;
			try {
				credentials = GoogleCredentials.fromStream(
						//new FileInputStream("C:/Users/User/Downloads/sparepartfactory-4d619-firebase-adminsdk-71jvb-f5547e041b.json"))
						new URL("http://35.240.160.9:8080/bmm-asset/berkahmm-78460-firebase-adminsdk-xwo8d-d9e22c8826.json").openStream())
						.createScoped(Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"));
						FirebaseOptions options = new FirebaseOptions.Builder()
						.setCredentials(credentials)
						.setDatabaseUrl("https://berkahmm-78460.firebaseio.com").build();

				FirebaseApp.initializeApp(options);
				init=true;
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		String listStringToken = mobSptDao.findSPTPhone(noHp);
		List<String> token = new ArrayList<>();
		token.add(listStringToken);
		if(listStringToken != null){
			MulticastMessage messageSales = MulticastMessage
					.builder()
					.setNotification(
							new Notification("BMM", "Ada Spt baru nih"))
					.addAllTokens(token)
					.setAndroidConfig(
							AndroidConfig
									.builder()
									.setNotification(
											AndroidNotification.builder()
													.setColor("#f15a24")
													.build()).build())
													.putData("noHp", noHp)
													.build();
			try {
				BatchResponse response = FirebaseMessaging.getInstance()
						.sendMulticast(messageSales);
				
			} catch (FirebaseMessagingException e) {
				e.printStackTrace();
				System.out.println("Notifikasi Gagal Dikirim");
			}
			
		}else{
			System.err.println("No telepon tidak ditemukan");
		}
	}	
} 