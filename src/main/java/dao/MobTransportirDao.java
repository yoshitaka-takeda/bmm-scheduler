package dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.MobTransportir;
import entity.MobTransportirPK;

public interface MobTransportirDao extends JpaRepository<MobTransportir, MobTransportirPK>{

	@Query("select count(a) from MobTransportir a where a.code = :code") 
	public Long countDataByTransCodeNo(@Param("code") String code);
	@Query("select a from MobTransportir a where a.code = :code") 
	public MobTransportir findOneTrans(@Param("code") String code);
}
