package entity;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the m_user database table.
 * 
 */
//user
@Entity
@Table(name="m_user")
@NamedQuery(name="MUser.findAll", query="SELECT m FROM MUser m")
public class MUser implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="email")
	private String email;

	@Column(name="created_by")
	private String createdBy;

	@Column(name="created_date")
	private Date createdDate;

	@Column(name="fullname")
	private String fullname;

	@Column(name="IMEI")
	private String imei;

	@Column(name="is_active")
	private byte isActive;

	@Column(name="modified_by")
	private String modifiedBy;

	@Column(name="modified_date")
	private Date modifiedDate;

	@Column(name="nipeg")
	private String nipeg;

	@Column(name="password")
	private String password;

	@Column(name="person_id")
	private int personId;

	@Column(name="phone_num")
	private String phoneNum;

	@Column(name="photo_text")
	private String photoText;

	@Column(name="pswd_default")
	private String pswdDefault;

	@Column(name="reference_code")
	private String referenceCode;

	@Column(name="token")
	private String token;

	@Column(name="username")
	private String username;

	public MUser() {
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getFullname() {
		return this.fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getImei() {
		return this.imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public byte getIsActive() {
		return this.isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getModifiedBy() {
		return this.modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Date getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getNipeg() {
		return this.nipeg;
	}

	public void setNipeg(String nipeg) {
		this.nipeg = nipeg;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getPersonId() {
		return this.personId;
	}

	public void setPersonId(int personId) {
		this.personId = personId;
	}

	public String getPhoneNum() {
		return this.phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getPhotoText() {
		return this.photoText;
	}

	public void setPhotoText(String photoText) {
		this.photoText = photoText;
	}

	public String getPswdDefault() {
		return this.pswdDefault;
	}

	public void setPswdDefault(String pswdDefault) {
		this.pswdDefault = pswdDefault;
	}

	public String getReferenceCode() {
		return this.referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public String getToken() {
		return this.token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}