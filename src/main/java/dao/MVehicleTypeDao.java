package dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.MVehicleType;

public interface MVehicleTypeDao extends JpaRepository<MVehicleType, Integer> {
	
	@Query("select count(a) from MVehicleType a "
			+ "	where trim(upper(a.vehicleTypeName)) = trim(upper(:vehicleName))")
	public abstract Long countDataByName(@Param("vehicleName") String vehicleName);
	
	@Query(value = "select a.id from m_vehicle_type a "
			+ "	where trim(upper(a.vehicle_type_name)) = trim(upper(:vehicleName)) "
			+ "	limit 1", nativeQuery = true)	
	public abstract Integer getIdByName(@Param("vehicleName") String vehicleName);
	
}
