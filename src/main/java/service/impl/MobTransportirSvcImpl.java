package service.impl;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import common.AttributeParam;

import dao.MobTransportirDao;
import entity.MobSpt;
import entity.MobTransportir;
import entity.MobTransportirPK;
import service.MobTransportirSvc;

@Service
@Transactional
public class MobTransportirSvcImpl implements MobTransportirSvc {

	@Autowired
	MobTransportirDao mobTransportirDao;

	@SuppressWarnings("static-access")
	@Override
	public void saveTransportir() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		HttpResponse response;
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(AttributeParam.hostToken);
		JSONObject jobj = new JSONObject();
		try {
			response = httpClient.execute(request);

			HttpEntity entity = response.getEntity();
			String responseString;
			responseString = EntityUtils.toString(entity);
			jobj = new JSONObject(responseString);
			@SuppressWarnings("unchecked")
			HashMap<String, Object> yourHashMap = new Gson().fromJson(
					jobj.get("data").toString(), HashMap.class);
			String a = yourHashMap.get("token").toString();

			//String url = "http://103.66.69.18:6026/sptservices/mobtransporter/list";
			HttpClient client2 = new DefaultHttpClient();
			HttpPost post = new HttpPost(AttributeParam.hostGetTrans);

			List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
			urlParameters.add(new BasicNameValuePair("token", a));

			post.setEntity(new UrlEncodedFormEntity(urlParameters));

			response = httpClient.execute(post);
			HttpEntity entity2 = response.getEntity();
			String responseString2 = EntityUtils.toString(entity2);

			jobj = new JSONObject(responseString2);

			JSONObject obj = jobj.getJSONObject("data");
			JSONArray joar = obj.getJSONArray("data");

			for (int i = 0; i < joar.length(); i++) {
				MobTransportir transportir = new MobTransportir();
				int error = 0;
				if (joar.getJSONObject(i).get("transporter_code") == null
						|| StringUtils.isEmpty(joar.getJSONObject(i).get(
								"transporter_code"))) {
					error++;
					System.err.println("Transporter code Null");
				} else {
					transportir.setCode((String) (joar.getJSONObject(i)
							.getString("transporter_code")));
				}
				if (joar.getJSONObject(i).get("transporter_name") == null
						|| StringUtils.isEmpty(joar.getJSONObject(i).get(
								"transporter_name"))) {
					error++;
					System.err.println("Transporter name Null");
				}else{
					transportir.setTransportirName((String) (joar.getJSONObject(i)
							.getString("transporter_name")));
				}
				if(joar.getJSONObject(i).get("created_by")==null || StringUtils.isEmpty(joar.getJSONObject(i).get("created_by"))){
					error++;
					System.err.println("created name Null");
				}else{

					transportir.setProcessedBy((String) joar.getJSONObject(i).getString("created_by"));
				}
				
				if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("created_date"))){
					error++;
					System.err.println("Error : created date is null");
					transportir.setProcessedDate(null);
				}else{
					try {
						transportir.setProcessedDate(sdf.parse(joar.getJSONObject(i).getString("created_date")));
					} catch (JSONException | ParseException e) {
						e.printStackTrace();
					}
				}
				
				if(joar.getJSONObject(i).get("is_active")==null || StringUtils.isEmpty(joar.getJSONObject(i).get("is_active"))){
					error++;
					System.err.println("is active Null");
				}else{

					transportir.setIsActive(((String) joar.getJSONObject(i).getString("is_active")).equals("Y")?true:false);
				}
				transportir.setId(0);

				if (mobTransportirDao.countDataByTransCodeNo(transportir
						.getCode()) > 0) {
//					 MobTransportirPK transportirPK = new MobTransportirPK();
//					 transportirPK.setCode(transportir.getCode());
					 transportir= mobTransportirDao.findOneTrans(transportir.getCode());
					 Boolean isActive = ((String) joar.getJSONObject(i).getString("is_active")).equals("Y")?true:false;
					 if(transportir.getIsActive()!=isActive){
						 transportir.setIsActive(isActive);
						 mobTransportirDao.save(transportir);
						 System.err.println("data di update");
						 System.out.println("");
					 }else{
						 System.err.println("data transportir sudah ada");
						 System.out.println("");
					 }
					
				} else {
					if (error > 0) {
						System.err
								.println("Data Gagal Disimpan, untuk transportir = "
										+ transportir.getCode());
					} else {
						mobTransportirDao.save(transportir);
						System.err.println("Data Tersimpan = "
								+ transportir.getCode() + ", "
								+ new Date().getTime());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
