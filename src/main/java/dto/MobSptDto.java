package dto;

import java.math.BigDecimal;
import java.util.Date;

public class MobSptDto {

	private int sptNo;
	private String addressShipFrom;
	private String addressShipTo;
	private String containerNo;
	private String created;
	private Date createdDate;
	private String custCode;
	private String custName;
	private int driverId;
	private byte isTransit;
	private String itemCode;
	private String itemName;
	private String ktpPicture;
	private String modified;
	private Date modifiedDate;
	private String phone;
	private BigDecimal qty;
	private BigDecimal qtySppb;
	private String referenceCode;
	private String shipFrom;
	private String sjNo;
	private int sppbKey;
	private int sppbNo;
	private Date sptDate;
	private Date sptExpiredDate;
	private String status;
	private String takeAssignmentBy;
	private Date takeAssignmentDate;
	private BigDecimal takeAssignmentLatitude;
	private BigDecimal takeAssignmentLongitude;
	private String transportirCode;
	private int transportirId;
	private String transportirName;
	private String vehicleNo;
	private int vehicleType;
	private BigDecimal weightBruto;
	private BigDecimal weightNetto;
	public int getSptNo() {
		return sptNo;
	}
	public void setSptNo(int sptNo) {
		this.sptNo = sptNo;
	}
	public String getAddressShipFrom() {
		return addressShipFrom;
	}
	public void setAddressShipFrom(String addressShipFrom) {
		this.addressShipFrom = addressShipFrom;
	}
	public String getAddressShipTo() {
		return addressShipTo;
	}
	public void setAddressShipTo(String addressShipTo) {
		this.addressShipTo = addressShipTo;
	}
	public String getContainerNo() {
		return containerNo;
	}
	public void setContainerNo(String containerNo) {
		this.containerNo = containerNo;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public String getCustCode() {
		return custCode;
	}
	public void setCustCode(String custCode) {
		this.custCode = custCode;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public int getDriverId() {
		return driverId;
	}
	public void setDriverId(int driverId) {
		this.driverId = driverId;
	}
	public byte getIsTransit() {
		return isTransit;
	}
	public void setIsTransit(byte isTransit) {
		this.isTransit = isTransit;
	}
	public String getItemCode() {
		return itemCode;
	}
	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getKtpPicture() {
		return ktpPicture;
	}
	public void setKtpPicture(String ktpPicture) {
		this.ktpPicture = ktpPicture;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public BigDecimal getQty() {
		return qty;
	}
	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}
	public BigDecimal getQtySppb() {
		return qtySppb;
	}
	public void setQtySppb(BigDecimal qtySppb) {
		this.qtySppb = qtySppb;
	}
	public String getReferenceCode() {
		return referenceCode;
	}
	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}
	public String getShipFrom() {
		return shipFrom;
	}
	public void setShipFrom(String shipFrom) {
		this.shipFrom = shipFrom;
	}
	public String getSjNo() {
		return sjNo;
	}
	public void setSjNo(String sjNo) {
		this.sjNo = sjNo;
	}
	public int getSppbKey() {
		return sppbKey;
	}
	public void setSppbKey(int sppbKey) {
		this.sppbKey = sppbKey;
	}
	public int getSppbNo() {
		return sppbNo;
	}
	public void setSppbNo(int sppbNo) {
		this.sppbNo = sppbNo;
	}
	public Date getSptDate() {
		return sptDate;
	}
	public void setSptDate(Date sptDate) {
		this.sptDate = sptDate;
	}
	public Date getSptExpiredDate() {
		return sptExpiredDate;
	}
	public void setSptExpiredDate(Date sptExpiredDate) {
		this.sptExpiredDate = sptExpiredDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTakeAssignmentBy() {
		return takeAssignmentBy;
	}
	public void setTakeAssignmentBy(String takeAssignmentBy) {
		this.takeAssignmentBy = takeAssignmentBy;
	}
	public Date getTakeAssignmentDate() {
		return takeAssignmentDate;
	}
	public void setTakeAssignmentDate(Date takeAssignmentDate) {
		this.takeAssignmentDate = takeAssignmentDate;
	}
	public BigDecimal getTakeAssignmentLatitude() {
		return takeAssignmentLatitude;
	}
	public void setTakeAssignmentLatitude(BigDecimal takeAssignmentLatitude) {
		this.takeAssignmentLatitude = takeAssignmentLatitude;
	}
	public BigDecimal getTakeAssignmentLongitude() {
		return takeAssignmentLongitude;
	}
	public void setTakeAssignmentLongitude(BigDecimal takeAssignmentLongitude) {
		this.takeAssignmentLongitude = takeAssignmentLongitude;
	}
	public String getTransportirCode() {
		return transportirCode;
	}
	public void setTransportirCode(String transportirCode) {
		this.transportirCode = transportirCode;
	}
	public int getTransportirId() {
		return transportirId;
	}
	public void setTransportirId(int transportirId) {
		this.transportirId = transportirId;
	}
	public String getTransportirName() {
		return transportirName;
	}
	public void setTransportirName(String transportirName) {
		this.transportirName = transportirName;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public int getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(int vehicleType) {
		this.vehicleType = vehicleType;
	}
	public BigDecimal getWeightBruto() {
		return weightBruto;
	}
	public void setWeightBruto(BigDecimal weightBruto) {
		this.weightBruto = weightBruto;
	}
	public BigDecimal getWeightNetto() {
		return weightNetto;
	}
	public void setWeightNetto(BigDecimal weightNetto) {
		this.weightNetto = weightNetto;
	}
	
	
}
