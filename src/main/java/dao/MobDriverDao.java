package dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.MobDriver;

public interface MobDriverDao extends JpaRepository<MobDriver, Integer> {
//driver
	@Query("select c.token from MUser c "
			+ ", MobDriver a "
			+ "where c.referenceCode=a.id "
			+ "and a.phoneNum=:phone")
	public String findTokenByPhone(@Param("phone") String phone);
}
