package service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.common.collect.Lists;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.google.gson.Gson;
import common.AttributeParam;

import dao.MVehicleTypeDao;
import dao.MobCustomerDao;
import dao.MobSptDao;
import entity.MVehicleType;
import entity.MobCustomer;
import entity.MobSpt;
import service.MobSptCmsSvc;
import service.NotificationSvc;

@Service
@Transactional
public class MobSptCmsSvcImpl implements MobSptCmsSvc {
	
	public Boolean init = false;
	
	@Autowired
	private MobSptDao mobSptDao;
	
	@Autowired
	private MobCustomerDao mobCustomerDao;
	
	@Autowired
	private NotificationSvc notificationSvc;
	
	@Autowired
	private MVehicleTypeDao mVehicleTypeDao;

	
	@SuppressWarnings("static-access")
	@Override
	public void saveData() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		HttpResponse response;
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(AttributeParam.hostToken);
		JSONObject jobj = new JSONObject();
		try {
			response = httpClient.execute(request);

			HttpEntity entity = response.getEntity();
			String responseString;
			responseString = EntityUtils.toString(entity);
			jobj = new JSONObject(responseString);
			@SuppressWarnings("unchecked")
			HashMap<String, Object> yourHashMap = new Gson().fromJson(
					jobj.get("data").toString(), HashMap.class);
			String a = yourHashMap.get("token").toString();
			try {
				//String url="http://103.66.69.18:6026/sptservices/mobspt/list";
				HttpClient client2 = new DefaultHttpClient();
				HttpPost post = new HttpPost(AttributeParam.hostGetSpt);

				List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
				urlParameters.add(new BasicNameValuePair("token", a));

				post.setEntity(new UrlEncodedFormEntity(urlParameters));
								
				response = httpClient.execute(post);
				HttpEntity entity2 = response.getEntity();
				String responseString2 = EntityUtils.toString(entity2);
				
				jobj = new JSONObject(responseString2);
				
								
				JSONObject obj = jobj.getJSONObject("data");
				JSONArray joar = obj.getJSONArray("data");
				
				if(init==false){
					GoogleCredentials credentials;
					try {
						credentials = GoogleCredentials.fromStream(
								//new FileInputStream("C:/Users/User/Downloads/sparepartfactory-4d619-firebase-adminsdk-71jvb-f5547e041b.json"))
								new URL(AttributeParam.urlJson).openStream())
								.createScoped(Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"));
								FirebaseOptions options = new FirebaseOptions.Builder()
								.setCredentials(credentials)
								.setDatabaseUrl("https://berkahmm-78460.firebaseio.com").build();

						FirebaseApp.initializeApp(options);
						init=true;
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				for (int i = 0; i < joar.length(); i++) {
					MobSpt spt = new MobSpt();
					int error = 0;
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("spt_no"))){
						error++;
						System.err.println("Error : Spt No is null");
						spt.setSptNo(0);
					}else{
						spt.setSptNo(Integer.parseInt(joar.getJSONObject(i).getString("spt_no"))) ;
					}
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("spt_date"))){
						error++;
						System.err.println("Error : Spt Date is null");
						spt.setSptDate(null);
					}else{
						try {
							spt.setSptDate(sdf.parse(joar.getJSONObject(i).getString("spt_date")));
						} catch (JSONException | ParseException e) {
							e.printStackTrace();
						}
					}
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("spt_expired_date"))){
						error++;
						System.err.println("Error : Spt Expired Date is null");
						spt.setSptExpiredDate(null);
					}else{
						try {
							spt.setSptExpiredDate(sdf.parse(joar.getJSONObject(i).getString("spt_expired_date")));
						} catch (JSONException | ParseException e) {
							e.printStackTrace();
						}
					}
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("sppb_no"))){
						spt.setSppbNo(0);
					}else{
						spt.setSppbNo(joar.getJSONObject(i).getInt("sppb_no"));
					}
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("cust_code"))){
						spt.setCustCode(null);
						error++;
					}else{
						spt.setCustCode(joar.getJSONObject(i).getString("cust_code"));
					}
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("cust_name"))){
						spt.setCustName(null);
						error++;
					}else{
						spt.setCustName(joar.getJSONObject(i).getString("cust_name"));
					}
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("address_ship_to"))){
						spt.setAddressShipTo(null);
						error++;
					}else{
						spt.setAddressShipTo(joar.getJSONObject(i).getString("address_ship_to"));
					}
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("transportir_code"))){
						error++;
						System.err.println("Error : Transportir Code Is Null");
						spt.setTransportirCode(null);
					}else{
						spt.setTransportirCode((String) joar.getJSONObject(i).get("transportir_code"));
					}
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("transportir_name"))){
						spt.setTransportirName(null);
					}else{
						spt.setTransportirName(joar.getJSONObject(i).getString("transportir_name"));
					}
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("item_code"))){
						spt.setTransportirName(null);
					}else{
						spt.setItemCode(joar.getJSONObject(i).getString("item_code"));
					}
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("item_name"))){
						spt.setItemName(null);
					}else{
						spt.setItemName(joar.getJSONObject(i).getString("item_name"));
					}
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("qty_sppb"))){
						spt.setQtySppb(null);
					}else{
						spt.setQtySppb(new BigDecimal(joar.getJSONObject(i).getString("qty_sppb")));
					}
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("vehicle_no"))){
						spt.setVehicleNo(null);
					}else{
						spt.setVehicleNo(joar.getJSONObject(i).getString("vehicle_no"));
					}
					
					String VehicleType=joar.getJSONObject(i).getString("vehicle_type");
					Integer idVehicle =mVehicleTypeDao.getIdByName(VehicleType);
					if(idVehicle==null){
						MVehicleType mVehicleType = new MVehicleType();
						mVehicleType.setIsActive(true);
						mVehicleType.setVehicleTypeName(VehicleType);
						mVehicleType.setProcessedBy("SYSTEM");
						mVehicleType.setProcessedDate(new Date());
						mVehicleType.setReferenceCode("0");
						mVehicleTypeDao.save(mVehicleType);
					}
					
					spt.setVehicleType(mVehicleTypeDao.getIdByName(VehicleType));
					
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("qty"))){
						spt.setQty(null);
					}else{
						spt.setQty(new BigDecimal(joar.getJSONObject(i).getString("qty")));
					}
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("phone"))|| joar.getJSONObject(i).getString("phone").equalsIgnoreCase("-")){
						spt.setPhone(null);
						error++;
					}else{
						spt.setPhone(joar.getJSONObject(i).getString("phone"));
					}
					String driverId=mobSptDao.findSPTIdPhone(spt.getPhone());
					if(driverId==null){
						error++;
						System.err.println("Error : Driver Id tidak ditemukan, untuk spt no = "+spt.getSptNo());
						spt.setDriverId(0);
					}else{
						spt.setDriverId(Integer.parseInt(driverId));
						System.err.println("Sukses : Driver Id ditemukan, Id = "+spt.getDriverId());
					}
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("container_no"))){
						spt.setContainerNo("Data Tidak Tersedia");
						System.err.println("error : container no is null");
					}else{
						spt.setContainerNo((String) joar.getJSONObject(i).get("container_no"));
						System.err.println("sukses : container no is not null");
					}
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("weight_netto"))){
						spt.setWeightNetto(new BigDecimal(joar.getJSONObject(i).getString("weight_netto")));
					}else{
						spt.setWeightNetto(new BigDecimal(joar.getJSONObject(i).getString("weight_netto")));
					}
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("weight_bruto"))){
						spt.setWeightBruto(null);
					}else{
						spt.setWeightBruto(new BigDecimal(joar.getJSONObject(i).getString("weight_bruto")));
					}
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("status"))){
						spt.setStatus(null);
					}else{
						spt.setStatus(joar.getJSONObject(i).getString("status"));
					}
					spt.setReferenceCode("1.0");
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("created"))){
						error++;
						System.err.println("created is null");
						spt.setCreated(null);
						spt.setModified(null);
					}else{
						spt.setCreated(joar.getJSONObject(i).getString("created"));
						spt.setModified(joar.getJSONObject(i).getString("created"));
					}
					if(joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("created_date"))){
						error++;
						System.err.println("error : created date is null");
						spt.setCreatedDate(null);
						spt.setModifiedDate(null);
					}else{
						spt.setCreatedDate(Timestamp.valueOf(joar.getJSONObject(i).getString("created_date")));
						spt.setModifiedDate(Timestamp.valueOf(joar.getJSONObject(i).getString("created_date")));
					}
					
					
					if(mobSptDao.countDataBySptNo(spt.getSptNo()) > 0){
//						spt = mobSptDao.findOne(spt.getSptNo());
//						boolean update = false;
//						if(!spt.getStatus().trim().equals(joar.getJSONObject(i).getString("status").trim())){
//							//System.err.println("Masuk edit");
//							spt.setStatus(joar.getJSONObject(i).getString("status").trim());
//							update = true;
//							//mobSptDao.save(spt);
//						}
//						if(!spt.getVehicleNo().trim().equals(joar.getJSONObject(i).getString("vehicle_no").trim())){
//							spt.setVehicleNo(joar.getJSONObject(i).getString("vehicle_no").trim());
//							update=true;
//						}
//						if(!spt.getContainerNo().trim().equals(joar.getJSONObject(i).getString("container_no").trim())){
//							spt.setContainerNo(joar.getJSONObject(i).getString("container_no").trim());
//							update=true;
//						}
//						if(!spt.getPhone().trim().equals(joar.getJSONObject(i).getString("phone").trim())){
//							String newDriverId =mobSptDao.findSPTIdPhone(joar.getJSONObject(i).getString("phone").trim());
//							if(newDriverId==null){
//								update=false;
//							}else{
//								update=true;
//								spt.setDriverId(Integer.parseInt(newDriverId));
//								spt.setPhone(joar.getJSONObject(i).getString("phone").trim());
//							}
//						}
//						if(update){
//							mobSptDao.save(spt);
//							System.out.println("Data di update di no Spt "+spt.getSptNo());
//						}else{
//							System.err.println("No Spt =  " + spt.getSptNo() +", sudah tersedia di database");
//						}
						if(error > 0){
							System.err.println("Data Gagal Update, Spt = "+spt.getSptNo());
						}else{

							MobSpt tempSpt = mobSptDao.getSptBysptNo(spt.getSptNo());
							spt.setTakeAssignmentLatitude(tempSpt.getTakeAssignmentLatitude());
							spt.setTakeAssignmentLongitude(tempSpt.getTakeAssignmentLongitude());
							spt.setTakeAssignmentDate(tempSpt.getTakeAssignmentDate());
							spt.setTakeAssignmentBy(tempSpt.getTakeAssignmentBy());
							spt.setModified(tempSpt.getModified());
							spt.setModifiedDate(tempSpt.getModifiedDate());
							spt.setIsTransit(tempSpt.getIsTransit());
							mobSptDao.save(spt);
							System.out.println("update untuk spt = "+ spt.getSptNo());
							if(spt.getCustCode().equals(spt.getTransportirCode())){
								System.err.println("tidak pakai mobile");
							}else if(spt.getDriverId()!=tempSpt.getDriverId() && !spt.getStatus().equalsIgnoreCase("C")){
								notificationSvc.getNotification(spt.getPhone(), spt.getSptNo(), 1);
							}
						}
						
					}else{
						if(error > 0){
							System.err.println("Data Gagal Disimpan, untuk no spt = "+spt.getSptNo());
						}else{
							if(mobCustomerDao.countDataByCustCode(spt.getCustCode())>0){
								System.err.println("customer sudah ada");
							}else{
								MobCustomer mobCustomer = new MobCustomer();
								mobCustomer.setCustCode(spt.getCustCode());
								mobCustomer.setCustName(spt.getCustName());
								mobCustomer.setAddress(spt.getAddressShipTo());
								mobCustomer.setIsActive(true);
								mobCustomer.setProcessedBy("SYSTEM");
								mobCustomer.setProcessedDate(new Date());
								mobCustomerDao.save(mobCustomer);
								System.out.println("data customer "+mobCustomer.getCustCode()+" berhasil disimpan");
							}
							spt.setIsTransit(false);
							mobSptDao.save(spt);
								//getNotification(spt.getPhone());
							
							if(!spt.getStatus().equalsIgnoreCase("C")){
								notificationSvc.getNotification(spt.getPhone(), spt.getSptNo(), 1);
							}else{
								System.err.println("Status close tidak kirim notif");
							}
							System.err.println("Data Tersimpan = "+spt.getSptNo()+", "+new Date().getTime());
						}
					}
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}catch (Exception e) {
				e.printStackTrace();
				System.err.println("Data gagal tersimpan ");
			}

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void getNotification(String noHp) {
		if(init==false){
			GoogleCredentials credentials;
			try {
				credentials = GoogleCredentials.fromStream(
						//new FileInputStream("C:/Users/User/Downloads/sparepartfactory-4d619-firebase-adminsdk-71jvb-f5547e041b.json"))
						new URL("http://api.berkahmm.com:8080/bmm-asset/berkahmm-78460-firebase-adminsdk-xwo8d-d9e22c8826.json").openStream())
						.createScoped(Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"));
						FirebaseOptions options = new FirebaseOptions.Builder()
						.setCredentials(credentials)
						.setDatabaseUrl("https://berkahmm-78460.firebaseio.com").build();

				FirebaseApp.initializeApp(options);
				init=true;
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		String listStringToken = mobSptDao.findSPTPhone(noHp);
		List<String> token = new ArrayList<>();
		token.add(listStringToken);
		if(listStringToken != null){
			MulticastMessage messageSales = MulticastMessage
					.builder()
					.setNotification(
							new Notification("BMM", "Ada Spt baru nih"))
					.addAllTokens(token)
					.setAndroidConfig(
							AndroidConfig
									.builder()
									.setNotification(
											AndroidNotification.builder()
													.setColor("#f15a24")
													.build()).build())
													.putData("noHp", noHp)
													.build();
			try {
				BatchResponse response = FirebaseMessaging.getInstance()
						.sendMulticast(messageSales);
				
			} catch (FirebaseMessagingException e) {
				e.printStackTrace();
				System.out.println("Notifikasi Gagal Dikirim");
			}
			
		}else{
			System.err.println("No telepon tidak ditemukan");
		}		
	}
	
	
}
