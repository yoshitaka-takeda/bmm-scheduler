package dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.MobSj;

public interface MobSjDao extends JpaRepository<MobSj, Integer> {
	
	@Query("select count(a) from MobSj a "
			+ "	where a.sjNo = :sjNo")
	public Long countDataBySjNo(@Param("sjNo") Integer sjNo);
	
	@Query(nativeQuery=true,value="select c.token "
			+ "from m_user c "
			+ "where c.phone_num=:phone ")
	public String findSPTPhone(@Param("phone") String phone);
	
	@Query(nativeQuery=true,value="select a.id "
			+ "from mob_driver a "
			+ "where a.phone_num=:phone ")
	public String findSPTIdPhone(@Param("phone") String phone);
	
	@Query("select a from MobSj a "
			+ "	where a.sjNo = :sjNo")
	public MobSj getDataBySjNo(@Param("sjNo") Integer sjNo);

	
}
