package service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;

import common.AttributeParam;
import dao.MVehicleTypeDao;
import dao.MobSjDao;
import entity.MVehicleType;
import entity.MobSj;
import entity.MobSpt;
import reactor.filter.FirstFilter;
import service.MobSjCmsSvc;
import service.NotificationSvc;

@Service
@Transactional
public class MobSjCmsSvcImpl implements MobSjCmsSvc {
	
	@Autowired
	private MobSjDao mobSjDao;
	
	@Autowired
	private MVehicleTypeDao mVehicleTypeDao;
	
	@Autowired
	private NotificationSvc notificationSvc;
	
	@Override
	public void saveData() {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		HttpResponse response;
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(AttributeParam.hostToken);
		JSONObject jobj = new JSONObject();
		try {
			response = httpClient.execute(request);

			HttpEntity entity = response.getEntity();
			String responseString;
			responseString = EntityUtils.toString(entity);
			jobj = new JSONObject(responseString);
			@SuppressWarnings("unchecked")
			HashMap<String, Object> yourHashMap = new Gson().fromJson(
					jobj.get("data").toString(), HashMap.class);
			String a = yourHashMap.get("token").toString();
			try {
				//String url="http://103.66.69.18:6026/sptservices/mobsj";
				HttpClient client2 = new DefaultHttpClient();
				HttpPost post = new HttpPost(AttributeParam.hostGetSJ);

				List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
				urlParameters.add(new BasicNameValuePair("token", a));

				post.setEntity(new UrlEncodedFormEntity(urlParameters));
								
				response = httpClient.execute(post);
				HttpEntity entity2 = response.getEntity();
				String responseString2 = EntityUtils.toString(entity2);
				
				jobj = new JSONObject(responseString2);
				
								
				JSONObject obj = jobj.getJSONObject("data");
				JSONArray joar = obj.getJSONArray("data");
				
				List<MobSpt> listSpt = new ArrayList<>();
				
				
				for (int i = 0; i < joar.length(); i++) {
					MobSj sj = new MobSj();
					int error = 0;
					if(joar.getJSONObject(i).get("sj_no") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("sj_no"))){
						error++;
						System.err.println("SJ No Null");
					}else{
						sj.setSjNo(Integer.parseInt(joar.getJSONObject(i).getString("sj_no")));
					}
					if(joar.getJSONObject(i).get("sj_date") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("sj_date"))){
						sj.setSjDate(null);
					}else{
						try {
							sj.setSjDate(sdf.parse(joar.getJSONObject(i).getString("sj_date")));
						} catch (JSONException e) {
							e.printStackTrace();
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					if(joar.getJSONObject(i).get("spt_no") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("spt_no"))){
						System.err.println(sj.getSjNo()+" no spt kosong");
						error++;
					}else{
						sj.setSptNo(Integer.parseInt(joar.getJSONObject(i).getString("spt_no")));
					}
					if(joar.getJSONObject(i).get("spm_no") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("spm_no"))){
						sj.setSpmNo(null);
					}else{
						sj.setSpmNo((String) joar.getJSONObject(i).get("spm_no"));
					}
					if(joar.getJSONObject(i).get("SEGEL1") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("SEGEL1"))){
						sj.setSegel1(null);
					}else{
						sj.setSegel1((String) joar.getJSONObject(i).get("SEGEL1"));
					}
					if(joar.getJSONObject(i).get("SEGEL2") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("SEGEL2"))){
						sj.setSegel2(null);
					}else{
						sj.setSegel2((String) joar.getJSONObject(i).get("SEGEL2"));
					}
					if(joar.getJSONObject(i).get("SEGEL3") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("SEGEL3"))){
						sj.setSegel3(null);
					}else{
						sj.setSegel3((String) joar.getJSONObject(i).get("SEGEL3"));
					}
					if(joar.getJSONObject(i).get("SEGEL4") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("SEGEL4"))){
						sj.setSegel4(null);
					}else{
						sj.setSegel4((String) joar.getJSONObject(i).get("SEGEL4"));
					}
					if(joar.getJSONObject(i).get("SEGEL5") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("SEGEL5"))){
						sj.setSegel5(null);
					}else{
						sj.setSegel5((String) joar.getJSONObject(i).get("SEGEL5"));
					}
					if(joar.getJSONObject(i).get("SEGEL6") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("SEGEL6"))){
						sj.setSegel6(null);
					}else{
						sj.setSegel6((String) joar.getJSONObject(i).get("SEGEL6"));
					}
					if(joar.getJSONObject(i).get("SEGEL7") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("SEGEL7"))){
						sj.setSegel7(null);
					}else{
						sj.setSegel7((String) joar.getJSONObject(i).get("SEGEL7"));
					}
					if(joar.getJSONObject(i).get("SEGEL8") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("SEGEL8"))){
						sj.setSegel8(null);
					}else{
						sj.setSegel8((String) joar.getJSONObject(i).get("SEGEL8"));
					}
					if(joar.getJSONObject(i).get("cust_code") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("cust_code"))){
						sj.setCustCode(null);
					}else{
						sj.setCustCode((String) joar.getJSONObject(i).get("cust_code"));
					}
					if(joar.getJSONObject(i).get("cust_name") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("cust_name"))){
						sj.setCustName(null);
					}else{
						sj.setCustName((String) joar.getJSONObject(i).get("cust_name"));
					}
					if(joar.getJSONObject(i).get("address_ship_to") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("address_ship_to"))){
						sj.setAddressShipTo(null);
					}else{
						sj.setAddressShipTo((String) joar.getJSONObject(i).get("address_ship_to"));
					}
					if(joar.getJSONObject(i).get("transporter_code") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("transporter_code"))){
						error++;
						System.err.println("error : Transportir Code Null ");
						sj.setTransportirCode(null);
					}else{
						sj.setTransportirCode((String) joar.getJSONObject(i).get("transporter_code"));
					}
					if(joar.getJSONObject(i).get("transporter_name") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("transporter_name"))){
						sj.setTransportirName(null);
					}else{
						sj.setTransportirName((String) joar.getJSONObject(i).get("transporter_name"));
					}
					if(joar.getJSONObject(i).get("address_ship_from") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("address_ship_from"))){
						sj.setAddressShipFrom(null);
					}else{
						sj.setAddressShipFrom((String) joar.getJSONObject(i).get("address_ship_from"));
					}
					if(joar.getJSONObject(i).get("item_code") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("item_code"))){
						sj.setItemCode(null);
					}else{
						sj.setItemCode((String) joar.getJSONObject(i).get("item_code"));
					}
					if(joar.getJSONObject(i).get("item_name") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("item_name"))){
						sj.setItemName(null);
					}else{
						sj.setItemName((String) joar.getJSONObject(i).get("item_name"));
					}
					if(joar.getJSONObject(i).get("qty_sppb") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("item_name"))){
						sj.setQtySppb(null);
					}else{
						sj.setQtySppb(new BigDecimal(joar.getJSONObject(i).getString("qty_sppb")));
					}
					if(joar.getJSONObject(i).get("vehicle_no") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("vehicle_no"))){
						sj.setVehicleNo(null);
					}else{
						sj.setVehicleNo((String) joar.getJSONObject(i).get("vehicle_no"));
					}
					if(joar.getJSONObject(i).get("vehicle_type") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("vehicle_type"))){
						sj.setVehicleType(0);
					}else{
						String vehicleName = joar.getJSONObject(i).getString("vehicle_type");
						if(mVehicleTypeDao.countDataByName(vehicleName) > 0){
							sj.setVehicleType(mVehicleTypeDao.getIdByName(vehicleName));
						}else{
							//insert new data to m_vehicle_type
							MVehicleType mVehicleType = new MVehicleType();
							mVehicleType.setVehicleTypeName(vehicleName);
							mVehicleType.setIsActive(true);
							mVehicleType.setProcessedBy("SYSTEM");
							mVehicleType.setProcessedDate(new Date());
							mVehicleTypeDao.save(mVehicleType);
							
							sj.setVehicleType(mVehicleTypeDao.getIdByName(mVehicleType.getVehicleTypeName()));
						}						
					}
					if(joar.getJSONObject(i).get("phone") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("phone"))){
						sj.setPhone(null);
					}else{
						sj.setPhone((String) joar.getJSONObject(i).get("phone"));
					}
					if(joar.getJSONObject(i).get("qty") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("qty"))){
						sj.setQty(null);
					}else{
						sj.setQty(new BigDecimal(joar.getJSONObject(i).getString("qty")));
					}
					if(joar.getJSONObject(i).get("container_no") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("container_no"))){
						sj.setContainerNo(null);
					}else{
						sj.setContainerNo((String) joar.getJSONObject(i).get("container_no"));
					}
					if(joar.getJSONObject(i).get("Status") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("Status"))){
						sj.setStatus(null);
					}else{
						sj.setStatus((String) joar.getJSONObject(i).get("Status"));
					}
					if(joar.getJSONObject(i).get("created") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("created"))){
						error++;
						System.err.println("error : Created is null");
						sj.setCreated(null);
						sj.setModified(null);
					}else{
						sj.setCreated((String) joar.getJSONObject(i).get("created"));
						sj.setModified((String) joar.getJSONObject(i).get("created"));
					}
					if(joar.getJSONObject(i).get("created_date") == null || StringUtils.isEmpty(joar.getJSONObject(i).get("created_date"))){
						error++;
						System.err.println("error : Created date is null");
						sj.setCreatedDate(null);
						sj.setModifiedDate(null);
					}else{
						try {
							sj.setCreatedDate(sdf.parse(joar.getJSONObject(i).getString("created_date")));
							sj.setModifiedDate(sdf.parse(joar.getJSONObject(i).getString("created_date")));
						} catch (JSONException e) {
							e.printStackTrace();
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					String driverId=mobSjDao.findSPTIdPhone(sj.getPhone());
					if(driverId==null){
						error ++;
						System.err.println("error : driver Id tidak ditemukan");
						sj.setDriverId(0);
					}else{
						sj.setDriverId(Integer.parseInt(driverId));
						System.err.println("query found : driver id ditemukan, id = "+sj.getDriverId());
					}
					//System.err.println("SJ: "+sj.getSjNo());
					//sSystem.err.println("ct: "+mobSjDao.countDataBySjNo(sj.getSjNo()));
					if(mobSjDao.countDataBySjNo(sj.getSjNo()) > 0){
						if(error>0){
							System.err.println("Data Gagal Update, untuk no Sj = "+sj.getSjNo());
						}else{
							MobSj tempSj = mobSjDao.getDataBySjNo(sj.getSjNo());
							sj.setDepartingFromLatitude(tempSj.getDepartingFromLatitude());
							sj.setDepartingFromLongitude(tempSj.getDepartingFromLongitude());
							sj.setDepartingFromDate(tempSj.getDepartingFromDate());
							sj.setDepartingFromBy(tempSj.getDepartingFromBy());
							sj.setFirstDestinationLatitude(tempSj.getFirstDestinationLatitude());
							sj.setFirstDestinationLongitude(tempSj.getFirstDestinationLongitude());
							sj.setFirstDestinationDate(tempSj.getFirstDestinationDate());
							sj.setFirstDestinationBy(tempSj.getFirstDestinationBy());
							sj.setTransitLatitude(tempSj.getTransitLatitude());
							sj.setTransitLongitude(tempSj.getTransitLongitude());
							sj.setTransitDate(tempSj.getTransitDate());
							sj.setTransitBy(tempSj.getTransitBy());
							sj.setDestinationLatitude(tempSj.getDestinationLatitude());
							sj.setDestinationLongitude(tempSj.getDestinationLongitude());
							sj.setDestinationDate(tempSj.getDestinationDate());
							sj.setDestinationBy(tempSj.getDestinationBy());
							sj.setPin(tempSj.getPin());
							sj.setModified(tempSj.getModified());
							sj.setModifiedDate(tempSj.getModifiedDate());
							if(!sj.getStatus().equalsIgnoreCase("L")){
								sj.setStatus(tempSj.getStatus());
							}
							//System.out.println("SJ: "+ sj.getSjNo());
							mobSjDao.save(sj);
							System.out.println("update untuk Sj = "+ sj.getSjNo() +" status sj : "+sj.getStatus());
						}
						
						//	System.err.println("No SJ Sama = "+sj.getSjNo() +", data tidak disimpan");
						
					}else{
						if(error > 0){
							System.err.println("Masih terjadi error : data tidak disimpan, no sj = "+sj.getSjNo());
						}else{
							Random rand = new Random();
							//System.err.println("Hiero");
							// Obtain a number between [0 - 10000].
							Integer random = rand.nextInt(10000);
							sj.setPin(random.toString());
							/* start modification */
							String phoneNum=sj.getPhone().toString();
							Integer phoneNumLength=phoneNum.length();
							System.out.println(phoneNum+" "+phoneNumLength);
							if((!sj.getCustCode().equals(sj.getTransportirCode()))||phoneNumLength>=10){
							/* end modification */
								notificationSvc.getNotification(sj.getPhone(), sj.getSjNo(), 2);
								//getNotification(sj.getPhone());
								mobSjDao.save(sj);
								System.err.println("Notifikasi berhasil dilakukan untuk SJ no = "+sj.getSjNo());
							}else{
								System.err.println("tidak pakai mobile");
							}
						}
					}
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	@Override
	public void getNotification(String noHp) {
		// TODO Auto-generated method stub
		
	}

}
