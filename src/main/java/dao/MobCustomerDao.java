package dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.MobCustomer;
import entity.MobCustomerPK;
import entity.MobTransportir;

public interface MobCustomerDao extends JpaRepository<MobCustomer, MobCustomerPK> {

	@Query("select count(a) from MobCustomer a where a.custCode = :code") 
	public Long countDataByCustCode(@Param("code") String code);
	
}
