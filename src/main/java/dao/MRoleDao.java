package dao;

import org.springframework.data.jpa.repository.JpaRepository;

import entity.MRole;

public interface MRoleDao extends JpaRepository<MRole, Integer> {

}
