package dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import entity.MobSpt;

public interface MobSptDao extends JpaRepository<MobSpt, Integer> {
	
	@Query("select count(a) from MobSpt a where a.sptNo = :sptNo") 
	public Long countDataBySptNo(@Param("sptNo") Integer sptNo);
	
	@Query(nativeQuery=true,value="select c.token "
			+ "from m_user c "
			+ "where c.phone_num=:phone ")
	public String findSPTPhone(@Param("phone") String phone);
	
	@Query(nativeQuery=true,value="select a.id from mob_driver a "
			+ "where a.phone_num=:phone ")
	public String findSPTIdPhone(@Param("phone") String phone);
	
	@Query("select a from MobSpt a where a.sptNo = :sptNo") 
	public MobSpt getSptBysptNo(@Param("sptNo") Integer sptNo);
	
	
	
	
}
