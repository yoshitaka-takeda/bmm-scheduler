package service.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.common.collect.Lists;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;

import dao.MobDriverDao;
import dao.MobSptDao;
import service.NotificationSvc;

@Service
@Transactional
public class NotificationSvcImpl implements NotificationSvc {

	@Autowired
	MobDriverDao mobDriverDao;
	
	
	//notification service
	@Override	
	public void getNotification(String noHp, Integer noSurat, Integer param) {
		/*System.err.println("No Hp = "+noHp);
		System.err.println("noSurat = "+noSurat);
		System.err.println("Param = "+param);*/
		String stringToken;
		try{
		 stringToken =mobDriverDao.findTokenByPhone(noHp);
		 System.err.println(stringToken);
		}catch(Exception e){
			stringToken=null;
			e.printStackTrace();
		}
		if(stringToken!=null){
			String msg="";
			switch (param) {
			case 1:
				msg="Ada Spt baru untuk anda dengan no "+noSurat;
				break;
			case 2:
				msg="Ada Surat jalan baru untuk anda dengan no "+noSurat;
				break;
			default:
				msg="Eror";
				break;
			}
			
			Message message = Message.builder()
					.setNotification(new Notification("BMM", msg))
					.setToken(stringToken)
					.setAndroidConfig(AndroidConfig
							.builder()
							.setNotification(
									AndroidNotification.builder()
									.build()).build())
									.putData("noSurat", noSurat.toString())
									.build();
			try {
				String response = FirebaseMessaging.getInstance().send(message);
				System.out.println("pesan berhasil di kirim "+response);
			} catch (FirebaseMessagingException e) {
				e.printStackTrace();
				System.out.println("Notifikasi Gagal Dikirim");
			}
		}else{
			System.out.println("no hp tidak ditemukan");
		}
	}

}
